var mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NoteSchema = new Schema({
    note: String,
    titre: String,
    priority: String,
    author: String
}, {
    timestamps:true
});


module.exports = mongoose.model("note", NoteSchema);