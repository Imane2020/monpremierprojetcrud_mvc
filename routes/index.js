const { query } = require('express');
var express = require('express');
var router = express.Router();
let NotesModel = require("../Model/NotesModel");

router.get('/', function(req, res, next) {
    res.render('index', { title: "Binevenue sur la page principale" , description: "Nous allons voir le CRUD, MVC et asynch"});
});
/////////////////////////////////////////
//////////////////////////////////////////
// SECTION NOTES //

/**read */
router.get('/notes/', function(req, res, next) {
  NotesModel.find({}).then(function(document){
    res.render('notes/index', {note: document });
  }
);
});
router.get('/notes/:id', function(req, res, next) {
    NotesModel.findById(req.params.id).then(function(document){
    res.render('notes/note', {note: document });
  }
);
});
/**Create */
router.get('/notes/add', function(req, res, next) {
  NotesModel.find({}).then(function(document){
    res.render('notes/add', {title:"Bienvenue sur la page d'ajout de note",
                              description: "ici Vous pouvez ajouter une nouvelle note",
                              note: document });
  }
);
});

router.post("/notes/add", function(req, res, next){
    let note_model = new NotesModel(req.body);
    note_model.save().then(note_model =>{
    // res.redirect("/notes/add");
    res.render('notes/add', {title:"Bienvenue sur la page d'ajout de note",
                              description: "ici Vous pouvez ajouter une nouvelle note",
                              reponse:"Ajout avec succès",
                              href: "retour a la liste des notes",
                            retour:"/notes" });
})
});


// FIN SECTION NOTES //
/////////////////////////////////////////
/////////////////////////////////////////
module.exports = router;

